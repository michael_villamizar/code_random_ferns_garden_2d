import lib.utils as utils
import lib.evaluation as eva
import lib.visualization as vis
import lib.samples_2d as dataset
import lib.random_ferns_2d as rfs
import lib.classifier_orfs_2d as orfs
import lib.classifier_brfs_2d as brfs
import lib.classifier_obrfs_2d as obrfs

# Print results.
def fun_print_results(results):
    ''' This function prints the classification results. '''

    # Messages.
    utils.fun_message('process', 'Results:')
    utils.fun_message('info', 'Equal error rate: {0:.3f}'. \
                      format(results['eer']['value']))
    utils.fun_message('info', 'Classifier threshold: {0:.3f}'. \
                      format(results['eer']['threshold']))
    utils.fun_message('info', 'Distance between classes: {0:.3f}'. \
                      format(results['distance']))
    utils.fun_message('info', 'Training time: {0:.3f} [sec.]'. \
                      format(results['train_time']))
    utils.fun_message('info', 'Test time: {0:.3f} [sec.]'. \
                      format(results['test_time']))

# Evaluation.
def fun_evaluation(scores, tst_labels, trn_time, tst_time):
    ''' This function evaluates the classifier on test samples.  '''

    # Classification results.
    utils.fun_message('process','Evaluating the classifier...')
    rates, eer, distance = eva.fun_classification_results(scores, tst_labels)

    # Classification results.
    results = {'rates':rates, 'eer':eer, 'distance':distance, 'scores':scores,\
              'train_time':trn_time, 'test_time':tst_time}

    # Print results.
    fun_print_results(results)

    return results

# Online random ferns classifier.
def fun_online_random_ferns_classifier(garden, num_ferns, trn_samples, \
                                       trn_labels, tst_samples, tst_labels):
    # Message.
    utils.fun_message('section','Online random ferns classifier')

    # Initialize and train the classifier.
    utils.fun_message('process','Initializing and training the classifier...')
    clfr, trn_time = orfs.fun_compute(garden, trn_samples, trn_labels, \
                                      num_ferns=num_ferns)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = orfs.fun_test(clfr, tst_samples)

    # Evaluation
    results = fun_evaluation(scores, tst_labels, trn_time, tst_time)

    return results

# Boosted random ferns classifier.
def fun_boosted_random_ferns_classifier(garden, num_weak_classifiers, \
                            trn_samples, trn_labels, tst_samples, tst_labels):
    # Message.
    utils.fun_message('section','Boosted random ferns classifier')

    # Initialize and train the classifier.
    utils.fun_message('process','Initializing and training the classifier...')
    clfr, trn_time = brfs.fun_compute(garden, trn_samples, trn_labels, \
                                  num_weak_classifiers=num_weak_classifiers)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = brfs.fun_test(clfr, tst_samples)

    # Evaluation
    results = fun_evaluation(scores, tst_labels, trn_time, tst_time)

    return results

# Online boosted random ferns classifier.
def fun_online_boosted_random_ferns_classifier(garden, num_weak_classifiers, \
                            trn_samples, trn_labels, tst_samples, tst_labels):
    # Message.
    utils.fun_message('section','Online boosted random ferns classifier')

    # Initialize and train the classifier.
    utils.fun_message('process','Initializing and training the classifier...')
    clfr, trn_time = obrfs.fun_compute(garden, trn_samples, trn_labels, \
                                  num_weak_classifiers=num_weak_classifiers)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = obrfs.fun_test(clfr, tst_samples)

    # Evaluation
    results = fun_evaluation(scores, tst_labels, trn_time, tst_time)

    return results

# Compare classification results.
def fun_compare_results(results):
    ''' This function compares the classification results of different
    classifiers on the test samples. '''

    # Recall-Precision curve.
    plots = {'orfs':{'x_value':results['orfs']['rates']['recall'] , \
                     'y_value':results['orfs']['rates']['precision']}, \
             'brfs':{'x_value':results['brfs']['rates']['recall'] , \
                     'y_value':results['brfs']['rates']['precision']}, \
             'obrfs':{'x_value':results['obrfs']['rates']['recall'] , \
                     'y_value':results['obrfs']['rates']['precision']}}

    # Plot recall-precision plots.
    vis.fun_show_figure(plots, 'Recall-Precision Plots', 'Recall', 'Precision')

    # F-score plots.
    plots = {'orfs':{'x_value':results['orfs']['rates']['thresholds'] , \
                     'y_value':results['orfs']['rates']['f_measure']}, \
             'brfs':{'x_value':results['brfs']['rates']['thresholds'] , \
                     'y_value':results['brfs']['rates']['f_measure']}, \
             'obrfs':{'x_value':results['obrfs']['rates']['thresholds'] , \
                     'y_value':results['obrfs']['rates']['f_measure']}}

    # Plot F-scores.
    vis.fun_show_figure(plots, 'F-score Plots', 'Threshold', 'F-score')

    # Score-class distribution distance -bars-.
    bars = {'orfs':{'Classifiers':results['orfs']['distance']}, \
            'brfs':{'Classifiers':results['brfs']['distance']}, \
            'obrfs':{'Classifiers':results['obrfs']['distance']}}

    # Show score-class distribution distance.
    vis.fun_show_bars(bars, 'Distribution Distance', '', 'Hellinger Distance')

    # Training/test times -bars-.
    bars = {'orfs':{'Train':results['orfs']['train_time'], \
                    'Test':results['orfs']['test_time']}, \
            'brfs':{'Train':results['brfs']['train_time'], \
                    'Test':results['brfs']['test_time']}, \
            'obrfs':{'Train':results['obrfs']['train_time'], \
                    'Test':results['obrfs']['test_time']}}

    # Show training/test times.
    vis.fun_show_bars(bars, 'Times', '', 'Seconds')

# Main function.
def fun_main():
    ''' This function computes and compares different classifiers based on
    ferns. '''

    # Message.
    utils.fun_message('heading', '2D Classification Problem')

    # Parameters.
    num_feats = 8  # Number of binary features.
    num_ferns = 50  # Number of random ferns.
    garden_size = 200  # Number of ferns in the garden -ferns pool-.
    num_weak_classifiers = 50  # Number of weak classifiers.

    # Train and test samples.
    trn_samples, trn_labels, num_classes = dataset.fun_example_2()
    tst_samples, tst_labels, num_classes = dataset.fun_example_2()

    # Computation of the fern "garden" -pool of random garden-.
    garden = rfs.fun_fern_garden_2d(num_ferns=garden_size, num_feats=num_feats)

    # Computation and evaluation of the online random ferns classifier.
    orfs_results = fun_online_random_ferns_classifier(garden, num_ferns, \
                              trn_samples, trn_labels, tst_samples, tst_labels)
    # Save results.
    utils.fun_save_data(orfs_results, './results/', 'orfs')

    # Computation and evaluation of the boosted random ferns classifier.
    brfs_results = fun_boosted_random_ferns_classifier(garden, \
       num_weak_classifiers, trn_samples, trn_labels, tst_samples, tst_labels)

    # Save results.
    utils.fun_save_data(brfs_results, './results/', 'brfs')

    # Computation and evaluation of the online boosted random ferns classifier.
    obrfs_results = fun_online_boosted_random_ferns_classifier(garden, \
       num_weak_classifiers, trn_samples, trn_labels, tst_samples, tst_labels)

    # Save results.
    utils.fun_save_data(obrfs_results, './results/', 'obrfs')

    # Classification results.
    results = {'orfs':orfs_results, 'brfs':brfs_results, 'obrfs':obrfs_results}

    # Compare classification results.
    fun_compare_results(results)

if __name__=='__main__':
    fun_main()
