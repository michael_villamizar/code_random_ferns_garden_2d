# Welcome to Random Ferns Garden 2D

This code trains and tests different classifiers based on random ferns for 2D
classification problems.  The classifiers included are: Online Random Ferns
(ORFs) [1], Boosted Random Ferns (BRFs) [2] and Online Boosted Random Ferns
(OBRFs) [3].

The classifiers are tested on several classification scenarios referred as
examples. For further information about these scenarios please refer to: 
https://bitbucket.org/michael_villamizar/code_samples_2d

## Dependencies
+ python
+ numpy
+ matplotlib

## Download
git clone https://michael_villamizar@bitbucket.org/michael_villamizar/code_random_ferns_garden_2d.git

## Usage
#### Run demo file:

```python
python demo.py
```

This demo file computes the addressed classifiers and compare them using
diverse evaluation metrics.

For example, the figure for the recall-precision plots is:

![Recall-precision plots](./imgs/fig_rp_comparison.png)

Similarly, the figure for the F-score/measures is:

![F-score plots](./imgs/fig_f-score_comparison.png)

The code also shows the training and test times for these classifiers:

![Training/test times](./imgs/fig_times_comparison.png)

#### Code example:

To compute and test the Boosted Random Ferns (BRFs) classifier:

```python
python lib/classifier_brfs_2d.py
```

This code runs the BRFs classfier and provides some classification results. For
example, the classifier confidence -score- over the test samples:

![Classification scores](./imgs/fig_classification_scores.png)

The  following figure depicts the classification results over the test samples.
Black samples correspond to wrongly classified samples.

![classification results](./imgs/fig_classification_results.png)

The classification confidence and uncertainty maps are also provided:

![classification map](./imgs/fig_classification_map.png)


![Uncertainty map](./imgs/fig_uncertainty_map.png)

## References

[1] **Fast keypoint recognition using random ferns**. M. Ozuysal, M. Calonder,
V.  Lepetit, and P. Fua.  Pattern Analysis and Machine Intelligence (PAMI). 2010.

[2] **Efficient rotation invariant object detection using boosted random
ferns**.  M. Villamizar, F. Moreno-Noguer, J. Andrade-Cetto and A. Sanfeliu.
Computer Vision and Pattern Recognition (CVPR). 2010.

[3] **Boosted Random Ferns for Object Detection**. M. Villamizar, J.
Andrade-Cetto, A. Sanfeliu and F. Moreno-Noguer. Pattern Analysis and Machine
Intelligence (PAMI). 2017.

## Citing
If you use this code please cite (or above references):

````
@article{Villamizar_pami2017,
author  = {M. Villamizar and J. Andrade and A. Sanfeliu and F. Moreno-Noguer},
title   = {Boosted Random Ferns for Object Detection},
journal = {IEEE Transactions on Pattern Analysis and Machine Intelligence (PAMI)},
volume  = {},
number  = {},
issn    = {0162-8828},
pages   = {},
doi     = {},
year    = {2017},
}
````

## License

```
Copyright (C) <2017> <Michael Villamizar>

This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy
of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Michael Villamizar. March, 2017.
Idiap Research Institute, Martigny, Switzerland.
```

## Authors
**Michael Villamizar**
[[https://www.idiap.ch/mvillamizar/]](https://www.idiap.ch/~mvillamizar/).
Idiap Research Institute.
Switzerland - 2017.
