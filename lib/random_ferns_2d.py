import numpy as np
import utils as utils
from numpy import random as rnd

# Random fern.
def fun_random_fern_2d(num_feats=7):
    ''' This function computes a random fern for 2D feature spaces. A fern is a
    set of binary features -decision stumps- in the 2D feature space which are
    computed randomly. '''

    # Allocate fern data.
    fern = np.zeros((num_feats, 2), dtype=float)

    ''' Binary features. Each feature is a decision stump in the 2D space and
    defined by an axe and a threshold. These parameters are computed at
    random.'''
    for f in range(num_feats):
        # Feature: axis (xi), with i={0,1}, and threshold (t).
        fern[f, 0] = np.floor(rnd.random()*2)  # xi
        fern[f, 1] = rnd.random()  # t

    return fern

# Random ferns.
def fun_random_ferns_2d(num_ferns=100, num_feats=7):
    ''' This function computes a set of ferns where each one is computed at
    random. The function returns an array with the fern data -binary features-
    '''

    # Random ferns computation.
    ferns = np.array([fun_random_fern_2d(num_feats) for r in range(num_ferns)])

    return ferns

# Fern garden.
def fun_fern_garden_2d(num_ferns=100, num_feats=7):
    '''This function computes a pool -garden- of random ferns where each one
    corresponds to a set of decision stumps -binary features- on the 2D feature
    space. The function returns a dictionary that includes the ferns and
    information.'''

    # Random ferns computation.
    ferns = fun_random_ferns_2d(num_ferns=num_ferns, num_feats=num_feats)

    # Fern garden.
    garden = {'ferns':ferns, 'num_ferns':num_ferns, 'num_feats':num_feats}

    return garden

# Test random fern.
def fun_test_fern_2d(fern, sample):
    ''' This function tests the input fern on the input 2D sample. The function
    returns the co-occurrence of the binary features -fern output or leaf-. '''

    # Variables.
    z = 0  # Fern output.
    num_feats = np.shape(fern)[0]  # Number of binary features.

    # Test binary features.
    for f in range(num_feats):

        # Feature {x0,x1} and threshold (t).
        x = int(fern[f, 0])
        t = fern[f, 1]

        # Update output.
        if sample[x]>t: z = z + 2**f

    return z

# Test random ferns.
def fun_test_random_ferns_2d(ferns, sample):
    ''' This function tests the input set of random ferns on the input sample.
    The function returns an array with the ferns outputs. '''

    # Variables.
    num_ferns = np.shape(ferns)[0]  # Number of ferns.

    # Test the random ferns. 
    outputs = np.array([fun_test_fern_2d(ferns[r, :, :], sample) \
                        for r in range(num_ferns)])
    return outputs

# Test fern garden.
def fun_test_fern_garden_2d(garden, sample):
    ''' This function tests the input fern garden that is a set/pool of random
    ferns. The function returns an array with the ferns outputs. '''

    # Test the fern garden.
    outputs = fun_test_random_ferns_2d(garden['ferns'], sample)

    return outputs

# Random fern outputs.
def fun_random_fern_outputs_2d(ferns, samples):
    ''' This function computes the outputs of the input set of random ferns on
    the input set of samples. The function returns an array with the ferns
    outputs.'''

    # Variables.
    num_samples = samples.shape[1]  # Number of samples.

    # Fern garden outputs.
    outputs = np.array([fun_test_random_ferns_2d(ferns, samples[:, n]) \
                        for n in range(num_samples)]).T
    return outputs

# Fern garden outputs.
def fun_fern_garden_outputs_2d(garden, samples):
    ''' This function computes the outputs of the input fern garden -pool of
    random ferns- on the input set of samples. The function returns an array
    with the ferns outputs.'''

    # Variables.
    num_samples = samples.shape[1]  # Number of samples.

    # Fern garden outputs.
    outputs = np.array([fun_test_fern_garden_2d(garden, samples[:, n]) \
                        for n in range(num_samples)]).T
    return outputs

# Main function.
def fun_main():
    '''This function computes and tests a fern garden for 2D feature spaces. A
    fern garden is a set/pool of ferns computed randomly. '''

    # Message.
    utils.fun_message('heading', 'Random Ferns')

    # Example: Computation of a fern garden.
    utils.fun_message('section', 'Computation of a fern garden' \
                      ' for 2D feature space.')

    # Parameters:
    num_ferns = 5  # Number of random ferns.
    num_feats = 7  # Number of binary features.

    # 2D test sample.
    x = np.array([0.5, 0.7])

    # Fern garden computation.
    garden = fun_fern_garden_2d(num_ferns=num_ferns, num_feats=num_feats)

    # Test the fern garden on the sample x.
    outputs = fun_test_fern_garden_2d(garden, x)

    # Messages.
    utils.fun_message('process','Fern garden information:')
    utils.fun_message('info','Number of ferns: {}'.format(garden['num_ferns']))
    utils.fun_message('info','Number of binary features: {}'. \
                      format(garden['num_feats']))
    utils.fun_message('process', 'Fern garden outputs (leaves):')
    utils.fun_message('info', '{}'.format(outputs))

if __name__=='__main__':
    fun_main()
