import numpy as np
import utils as utils
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import random as rnd

# Generate color.
def fun_generate_color(max_color_value=255):
    ''' This function generates colors at random. '''

    # Random color.
    rnd_color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: rnd.randint(0, \
                                                  max_color_value), range(3)))
    return rnd_color

# Show bars.
def fun_show_bars(data, title=None, x_label=None, y_label=None):
    ''' This function shows a figure with the input data using bars.'''

    # Parameters.
    bar_width = 0.2

    # Show bars.
    cont = 0  # Counter.
    for key, value in data.items():
        xticks = data[key].keys()  # x ticks.
        values = data[key].values()  # Bar values.
        index = np.arange(len(values))  # x ticks index.
        rnd_color = fun_generate_color(230)  # Random color.
        plt.bar(left=index + cont*bar_width, height=values, width=bar_width,\
                color=rnd_color, label=key)  # Bar.
        cont += 1  # Update counter.

    # Plot attributes.
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.xticks(index + bar_width*cont*0.5, xticks)
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.show()

# Show figure.
def fun_show_figure(data, title=None, x_label=None, y_label=None):
    ''' This function shows a figure with input data.'''

    # Plot.
    for key, value in data.items():
        x_value = data[key]['x_value']
        y_value = data[key]['y_value']
        plt.plot(x_value, y_value, label=key, linewidth=4)
    plt.legend(title="Legend", fancybox=True, loc=3)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.grid()
    plt.show()

# Show classifier confidence -score-.
def fun_show_scores(scores, labels):
    ''' This function shows the classifier confidence -score- over samples.'''

    # Variables.
    num_classes = np.max(labels) + 1  # Number of classes.

    # Plot data.
    plots = {}
    for c in range(num_classes):
        key = 'Class={}'.format(c)  # Plot key.
        indx = labels == c  # Samples indexes for class c.
        num_samples = np.sum(indx)  # Number of samples for class c.
        plots[key] = {'x_value':np.arange(num_samples), \
                      'y_value':scores[indx]}

    # Show figure.
    fun_show_figure(plots, 'Classification Scores', 'Sample', 'Score')

# Show recall-precision curve.
def fun_show_rp_curve(rates):
    '''This functions plots the recall-precision curve using the classification
    rates.'''

    # Plot data.
    plots = {'RP-plot':{'x_value': rates['recall'],\
                        'y_value':rates['precision']}}

    # Show figure.
    fun_show_figure(plots, 'Recall-Precision Curve', 'Recall', 'Precision')

# Show the f-score curve.
def fun_show_f_score(rates):
    '''This functions plots the f-measure/score using the classification
    rates.'''

    # Plot data.
    plots = {'F-score':{'x_value': rates['thresholds'],\
                        'y_value': rates['f_measure']}}

    # Show figure.
    fun_show_figure(plots, 'F-score Curve', 'Threshold', 'Value')

# Show classification rates.
def fun_show_rates(rates):
    '''This function shows the true positives, false positives and false
    negatives in terms of the classifier threshold. '''

    # Plot data.
    plots = {'True positives':{'x_value':rates['thresholds'], \
                               'y_value':rates['true_positives']}, \
             'False positives':{'x_value':rates['thresholds'], \
                                'y_value':rates['false_positives']}, \
             'False negatives':{'x_value':rates['thresholds'], \
                                'y_value':rates['false_negatives']}}

    # Show figure.
    fun_show_figure(plots, 'Classification Rates', 'Threshold', 'Value')

# Show classification map.
def fun_show_classification_map(clfr, fun_test, num_steps=30):
    ''' This function computes the classification maps: the confidence map and
    the uncertainty map.'''

    # Feature step.
    step = 1.0/num_steps

    # Feature space.
    space = np.array([[(u,v) for u in np.arange(0, 1, step)] \
                      for v in np.arange(0, 1, step)])

    # Test the classifier.
    scores, _ = fun_test(clfr, np.reshape(space, (num_steps*num_steps,2)).T)

    # Entropy.
    hstm = np.vstack((1-scores, scores))
    entropy = -1*np.sum(hstm*np.log(hstm), axis=0)

    # Classification and uncertainty maps.
    clf_map = np.reshape(scores, (num_steps, num_steps))
    unc_map = np.reshape(entropy, (num_steps, num_steps))

    # Show classification map.
    plt.imshow(clf_map)
    plt.gca().invert_yaxis()
    plt.title('Classification Map')
    plt.xticks([], [])
    plt.yticks([], [])
    plt.xlabel('x0')
    plt.ylabel('x1')
    plt.show()

    # Show uncertainty map.
    plt.imshow(unc_map)
    plt.gca().invert_yaxis()
    plt.title('Uncertainty Map')
    plt.xticks([], [])
    plt.yticks([], [])
    plt.xlabel('x0')
    plt.ylabel('x1')
    plt.show()

# Show samples.
def fun_show_results(samples, labels, scores, threshold=0.5):
    ''' This function shows the positive and negative samples in the 2D feature
    space. '''

    # Number of samples classes.
    num_classes = np.max(labels) + 1

    # Assert: This function only works for two classes (positive and negative
    # classes).
    assert num_classes==2

    # Plot samples. Red: true positives. Blue: true negatives.
    indx = np.logical_and(labels == 1 , scores > threshold)
    plt.plot(samples[0, indx], samples[1, indx], 'go')
    indx = np.logical_and(labels == 1 , scores <= threshold)
    plt.plot(samples[0, indx], samples[1, indx], 'ko')
    indx = np.logical_and(labels == 0 , scores <= threshold)
    plt.plot(samples[0, indx], samples[1, indx], 'bo')
    indx = np.logical_and(labels == 0 , scores > threshold)
    plt.plot(samples[0, indx], samples[1, indx], 'ko')

    # Plot figure.
    plt.axis('equal')
    plt.xlabel('x0')
    plt.ylabel('x1')
    plt.title('Classification Results')
    plt.grid()
    plt.show()

# Show two-class distributions.
def fun_show_class_distributions(scores, labels):
    '''This function shows the score distributions -Gaussian distributions- for
    two classes (e.g, positive and negative classes). '''

    # Number of classes.
    num_classes = np.max(labels) + 1

    # Assert: This function only works for two classes (positive and negative
    # classes).
    assert num_classes==2

    # Variable.
    x = np.arange(0, 1, 0.01)

    # Positive and negative indexes.
    pos_indx = labels==1
    neg_indx = labels==0

    # Score Gaussian distributions.
    pos_miu = np.mean(scores[pos_indx])
    neg_miu = np.mean(scores[neg_indx])
    pos_std = np.std(scores[pos_indx])
    neg_std = np.std(scores[neg_indx])

    # Positive and negative distributions.
    pos_distribution = np.exp(-np.power(x - pos_miu, 2.) / \
                              (2*np.power(pos_std, 2.)))
    neg_distribution = np.exp(-np.power(x - neg_miu, 2.) / \
                              (2*np.power(neg_std, 2.)))

    # Plot figure.
    plt.plot(x, neg_distribution, label='Negative class', linewidth=4)
    plt.plot(x, pos_distribution, label='Positive class', linewidth=4)
    plt.legend()
    plt.axis('equal')
    plt.xlabel('Score')
    plt.ylabel('Value')
    plt.title('Score Class Distributions')
    plt.grid()
    plt.show()

# Main.
def fun_main():
    '''This file includes functions to visualize classification results. '''

    # Message.
    utils.fun_message('heading','Visualization')

if __name__=='__main__':
    fun_main()
