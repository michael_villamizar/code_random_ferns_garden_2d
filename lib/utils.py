import os
import pickle

# Print messages.
def fun_message(message_id='heading', text='my World', space=2):
    ''' This function prints a message. '''

    # Message: heading.
    if message_id=='heading':
        print('\n===========================================')
        print('Welcome to {}'.format(text))
        print('Michael Villamizar')
        print('Idiap Research Institute')
        print('Martigny, Switzerland - 2017')
        print('===========================================\n')

    # Message: process.
    if message_id=='process': print('+ {}'.format(text))

    # Message: information.
    if message_id=='info': print('{0}{1}'.format(space*' ', text))

    # Message: line separator.
    if message_id=='line': print('===========================================')

    # Message: section.
    if message_id=='section':
        print('\n===========================================')
        print('{}'.format(text))
        print('===========================================')

# Save data.
def fun_save_data(data, path, text='data', show=True):
    ''' This function saves the input data using the input path. '''

    # Message.
    if show: fun_message('process', 'Saving {} ...'.format(text))

    # Check path.
    if not os.path.exists(path): os.makedirs(path)

    # Save data.
    pickle.dump(data, open(path+text+'.p', 'wb'))

    # Message.
    if show: fun_message('info', '{} saved in {}'.format(text, path))

# Main.
def fun_main():
    ''' This file includes diverse functions to compute classifiers. '''

    # Message.
    fun_message('heading','Utils')

if __name__=='__main__':
    fun_main()
