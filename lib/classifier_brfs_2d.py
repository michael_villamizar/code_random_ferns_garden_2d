import time
import numpy as np
import utils as utils
import evaluation as eva
import visualization as vis
import samples_2d as dataset
import random_ferns_2d as rfs
from numpy import random as rnd

# Initialization.
def fun_init(garden, num_weak_classifiers=10):
    ''' This function initializes the boosted random ferns classifier with the
    input garden -pool of random ferns-. The most discriminative random ferns
    from the garden are chosen via boosting to build the classifier.'''

    # Variables.
    num_feats = garden['num_feats']  # Number of binary features.
    garden_size = garden['num_ferns']  # Number of ferns in the garden.

    # Allocate initial ferns distributions -empty histograms-.
    num_bins = 2**num_feats  # Number of histogram bins.
    hstms = np.zeros((num_weak_classifiers, num_bins), dtype=float)

    # Ferns selection. Initially, the ferns are chosen from the garden at
    # random, but then they are selected from the garden using boosting.
    rnd_indexes = range(garden_size)  # Random indexes.
    rnd.shuffle(rnd_indexes)  # Shuffle.
    rnd_indexes = rnd_indexes[:num_weak_classifiers]  # Select the first ferns.
    ferns = garden['ferns'][rnd_indexes, :, :]  # Extract random ferns.

    # Classifier info.
    info = {'num_weak_classifiers':num_weak_classifiers, \
            'num_feats':num_feats, 'num_bins':num_bins}

    # Classifier: boosted random ferns.
    clfr = {}  # classifier
    clfr['tag'] = 'brfs'  # Classifier tag.
    clfr['info'] = info  # Classifier information.
    clfr['ferns'] = ferns  # Random ferns.
    clfr['hstms'] = hstms  # Fern probabilities.

    return clfr

# Compute ferns distributions.
def fun_fern_distributions(weights, ferns_outputs, labels, num_bins, \
                           fern_index):
    ''' This function computes the positive and negative fern distributions
    -histograms- for the input fern index. The distributions are computed
    according to the samples weights.'''

    # Number of samples.
    num_samples = np.shape(ferns_outputs)[1]

    # Positive and negative fern distributions -histograms-.
    eps = 0.000001  # Small quantity.
    pos_hstm = eps*np.ones(num_bins, dtype=float)
    neg_hstm = eps*np.ones(num_bins, dtype=float)

    # Samples.
    for n in range(num_samples):

        # Fern output.
        z = ferns_outputs[fern_index, n]

        # Update fern distributions.
        if (labels[n]==1): pos_hstm[z] += weights[n]
        if (labels[n]==0): neg_hstm[z] += weights[n]

    # Normalization.
    pos_hstm /= np.sum(pos_hstm)
    neg_hstm /= np.sum(neg_hstm)

    return pos_hstm, neg_hstm

# Weak learner.
def fun_weak_learner(weights, labels, garden_outputs, indexes, num_bins):
    ''' This function computes the weak learner. The weak learner is a random
    fern that best discriminates the positive samples from the negative ones
    under the input distribution of samples weights. '''

    # Variables.
    best_fern = -1  # Best fern index. 
    best_distance = 10000  # Best two-class distribution distance.

    # Number of ferns in the garden.
    num_ferns = np.shape(garden_outputs)[0]

    # Random ferns.
    for r in range(num_ferns):

        # Positive and negative fern distributions.
        pos_hstm, neg_hstm = fun_fern_distributions(weights, garden_outputs,\
                                                    labels, num_bins, r)
        # Battacharyya distance.
        distance = 2*np.sum(pos_hstm*neg_hstm)

        # Best fern -minimum distance and not repeated fern-.
        if distance<best_distance:
            if r not in indexes:

                # Update best fern.
                best_fern = r  # Fern index.
                best_distance = distance  # Battacharyya distance.

                # Ratio of fern distributions.
                hstm = pos_hstm/(pos_hstm + neg_hstm)

    # Assert.
    assert best_fern>=0

    # Update selected fern indexes.
    indexes.append(best_fern)

    # Weak classifier.
    weak_classifier = {'fern_index':best_fern, 'distance':distance, \
                       'hstm':hstm}
    return weak_classifier

# Test weak classifier.
def fun_test_weak_classifier(weak_classifier, garden_outputs):
    ''' This function tests the weak classifier on the samples. Here, we use
    the garden outputs to speed up the training. The function returns the
    confidences -classification scores- of the current weak classifier. '''

    # Number of samples.
    num_samples = np.shape(garden_outputs)[1]

    # Allocate.
    scores = np.zeros(num_samples, dtype=float)

    # Samples.
    for n in range(num_samples):

        # Fern output.
        z = garden_outputs[weak_classifier['fern_index'], n]

        # Update fern distributions.
        scores[n] = weak_classifier['hstm'][z]

    return scores

# Add weak classifier.
def fun_add_weak_classifier(clfr, garden, weak_classifier, \
                            weak_classifier_index):
    ''' This function adds the input weak classifier to the boosted random
    ferns classifier. '''

    # Weak classifier attributes.
    fern_hstm = weak_classifier['hstm']  # Fern -ratio- distribution.
    fern_index = weak_classifier['fern_index']  # Selected fern index.

    # Store the computed fern -ratio- distribution.
    clfr['hstms'][weak_classifier_index, :] = fern_hstm

    # Store the fern data from the pool of random ferns.
    clfr['ferns'][weak_classifier_index, :, :] = \
            garden['ferns'][fern_index, :, :]

# Update weights.
def fun_update_weights(weights, scores, labels, max_weight=100):
    ''' This function updates the distribution of samples weights according to
    the classification scores provided by the weak classifier. '''

    # Number of samples.
    num_samples = len(labels)

    # Update samples weights.
    weights *= np.exp(-4.0*(labels-1)*(scores-0.5))

    # Threshold weights to remove outliers.
    weights = np.maximum(weights, float(max_weight)/num_samples)

    # Normalize weights.
    weights /= np.sum(weights)

# Train the boosted random ferns classifier.
def fun_train(clfr, garden, samples, labels):
    ''' This function trains the boosted random ferns classifier. This
    classifier is computed from the combination of weak classifiers. Each weak
    classifier is a random fern chosen from the input garden via boosting. The
    most discriminative ferns are selected to build the classifier.'''

    # Assert -classifier type-.
    assert clfr['tag']=='brfs'

    # Number of samples.
    num_samples = samples.shape[1]

    # Assert -number of samples-.
    assert num_samples==len(labels)

    # Time.
    t0 = time.time()

    # Message.
    utils.fun_message('process','Boosting')

    # Samples weights.
    weights = np.ones(num_samples, dtype=float)/num_samples

    # Selected fern indexes.
    indexes = []

    # Fern garden outputs -computed in advance to keep efficiency-.
    garden_outputs = rfs.fun_fern_garden_outputs_2d(garden, samples)

    # Weak classifier computation.
    for w in range(clfr['info']['num_weak_classifiers']):

        # Computation of the weak classifier.
        weak_classifier = fun_weak_learner(weights, labels, garden_outputs, \
                                           indexes, clfr['info']['num_bins'])
        # Assemble weak classifier.
        fun_add_weak_classifier(clfr, garden, weak_classifier, w)

        # Test weak classifier.
        scores = fun_test_weak_classifier(weak_classifier, garden_outputs)

        # Updates weights.
        fun_update_weights(weights, scores, labels)

        # Message.
        if w%20==0: utils.fun_message('info','Weak classifier: {}/{}'.\
                              format(w, clfr['info']['num_weak_classifiers']))

    # Training time.
    trn_time = time.time()- t0

    return trn_time

# Classifier computation.
def fun_compute(garden, samples, labels, num_weak_classifiers=10):
    ''' This function computes -initialize and train- the boosted random ferns
    classifier.'''

    # Initialize the classifier.
    clfr = fun_init(garden, num_weak_classifiers=num_weak_classifiers)

    # Train the classifier.
    trn_time = fun_train(clfr, garden, samples, labels)

    return clfr, trn_time

# Test the classifier.
def fun_test_classifier(clfr, sample):
    ''' This function tests the classifier on the input sample. '''

    # Assert -check classifier-.
    assert clfr['tag']=='brfs'

    # Ferns outputs.
    ferns_outputs = rfs.fun_test_random_ferns_2d(clfr['ferns'], sample)

    # Variables.
    score = 0.0  # Classification score.

    # Test weak classifiers.
    for w in range(clfr['info']['num_weak_classifiers']):

        # Fern output.
        z = ferns_outputs[w]

        # Classifier confidence.
        score += clfr['hstms'][w, z]

    # Normalization.
    score /= clfr['info']['num_weak_classifiers']

    return score

# Test stage.
def fun_test(clfr, samples):
    ''' This function tests the classifier on the input set of samples. '''

    # Assert -check classifier-.
    assert clfr['tag']=='brfs'

    # Time.
    t0 = time.time()

    # Number of test samples.
    num_samples = samples.shape[1]

    # Test the classifier on the samples.
    scores = np.array([fun_test_classifier(clfr, samples[:, n]) \
                       for n in range(num_samples)])

    # Test time.
    tst_time = time.time()- t0

    return scores, tst_time

# Main.
def fun_main():
    ''' This function computes the Boosted Random Ferns classifier (BRFs) for
    the classification of 2D samples belonging to two different classes. '''

    # Message.
    utils.fun_message('heading', 'Boosted Random Ferns Classifier')
    utils.fun_message('section', 'Boosted Random Ferns Classifier')

    # Parameters:
    num_feats = 8  # Number of binary features.
    num_weak_classifiers = 50  # Number of weak classifiers.
    garden_size = 200  # Number of ferns in the garden -ferns pool-.

    # Train and test samples.
    trn_samples, trn_labels, num_classes = dataset.fun_example_2()
    tst_samples, tst_labels, num_classes = dataset.fun_example_2()

    # Computation of the fern "garden" -pool of random garden-.
    garden = rfs.fun_fern_garden_2d(num_ferns=garden_size, num_feats=num_feats)

    # Computation of the classifier.
    utils.fun_message('process','Initializing and training...')
    clfr, trn_time = fun_compute(garden, trn_samples, trn_labels, \
                                 num_weak_classifiers=num_weak_classifiers)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = fun_test(clfr, tst_samples)

    # Classification results.
    utils.fun_message('process','Evaluating the classifier...')
    rates, eer, distance = eva.fun_classification_results(scores, tst_labels)

    # Messages.
    utils.fun_message('process', 'Results:')
    utils.fun_message('info', 'Equal error rate: {0:.3f}'.format(eer['value']))
    utils.fun_message('info', 'Classifier threshold: {0:.3f}'. \
                      format(eer['threshold']))
    utils.fun_message('info', 'Distance between classes: {0:.3f}'. \
                      format(distance))
    utils.fun_message('info', 'Training time: {0:.3f} [sec.]'.format(trn_time))
    utils.fun_message('info', 'Test time: {0:.3f} [sec.]'.format(tst_time))

    # Results -visualization-.
    vis.fun_show_scores(scores, tst_labels)  # Classifier confidence.
    vis.fun_show_rp_curve(rates)  # Recall-precision curve.
    vis.fun_show_f_score(rates)  # F-measure/score.
    vis.fun_show_rates(rates)  # True/false positives and false negative rates.
    vis.fun_show_classification_map(clfr, fun_test)  # Classification maps.
    vis.fun_show_results(tst_samples, tst_labels, scores, eer['threshold'])
    vis.fun_show_class_distributions(scores, tst_labels) # Score distributions.

if __name__=='__main__':
    fun_main()
