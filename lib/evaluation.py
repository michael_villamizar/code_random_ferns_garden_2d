import numpy as np
import utils as utils

# Hellinger distance.
def fun_hellinger_distance(pos_miu, neg_miu, pos_var, neg_var):
    '''This function computes the Hellinger distance between two Gaussian
    distributions.'''

    # Hellinger distance.
    k1 = 2*np.sqrt(pos_var)*np.sqrt(neg_var);
    k2 = pos_var + neg_var;
    k3 = (pos_miu-neg_miu)**2;
    distance = 1 - np.sqrt(k1/k2)*np.exp(-0.25*k3/k2);

    return distance

# Score distribution distance.
def fun_score_distribution_distance(scores, labels):
    '''This function computes the distance between the positive and negative
    score distributions, indicating the degree of separability between these
    two classes.'''

    # Number of classes.
    num_classes = np.max(labels) + 1

    # Assert: This function only works for two classes (positive and negative
    # classes).
    assert num_classes==2

    # Positive and negative indexes.
    pos_indx = labels==1
    neg_indx = labels==0

    # Score Gaussian distributions.
    pos_miu = np.mean(scores[pos_indx])
    neg_miu = np.mean(scores[neg_indx])
    pos_var = np.std(scores[pos_indx])**2
    neg_var = np.std(scores[neg_indx])**2

    # Hellinger distance.
    distance = fun_hellinger_distance(pos_miu, neg_miu, pos_var, neg_var)

    # Check empty distributions.
    if (pos_var==0 or neg_var==0): distance = 0

    return distance

# Recall, precision and f-score/measure rates.
def fun_recall_precision_rates(tps=0, fps=0, fns=0):
    ''' This function computes the recall, precision and f-score/measure rates
    using the input numbers of true positives, false positives and false
    negatives.  '''

    # Variables.
    rec = 0.0  # Recall.
    pre = 0.0  # Precision.
    fme = 0.0  # F-score/measure.

    # Classification rates.
    if tps>0:
        rec = float(tps)/float(tps + fns)  # Recall.
        pre = float(tps)/float(tps + fps)  # Precision.
        fme = 2.0*rec*pre/(rec+pre)  # F-measure.

    return rec, pre, fme

# Classification rates.
def fun_classification_rates(scores, labels, num_iterations=1000):
    '''This function computes some classification rates of the classifier over
    samples. The function uses the samples scores -classifier confidence- with
    the samples labels to compute these classification rates. Specifically, the
    function computes the precision, recall and f-measure plots, and the equal
    error rate (EER), that is the point where recall and precision are equal.
    The function also computes true positives, false positives and false
    negatives rates.'''

    # Number of classes
    num_classes = np.max(labels) + 1

    # Assert: By the moment, this function only works for two classes (positive
    # and negative classes).
    assert num_classes==2

    # Positive and negative indexes.
    pos_indx = labels==1
    neg_indx = labels==0

    # Minimum and maximum threshold values.
    min_thr = np.min(scores[:])
    max_thr = np.max(scores[:])

    # Threshold step.
    step = float(max_thr-min_thr)/num_iterations

    # variables.
    counter = 0  # Counter.
    data = np.zeros((num_iterations, 7))  # Classification data.

    # Threshold.
    for thr in enumerate(np.arange(min_thr, max_thr, step)):

        # Check possible error.
        if thr[0]==num_iterations: break

        # Rates.
        tps = np.sum(scores[pos_indx]>thr[1])  # True positives.
        fps = np.sum(scores[neg_indx]>thr[1])  # False positives.
        fns = np.sum(scores[pos_indx]<=thr[1])  # False negatives.

        # Recall, precision and f-score/measure rates.
        rec, pre, fme = fun_recall_precision_rates(tps, fps, fns)

        # Save values.
        data[thr[0], :] = rec, pre, fme, tps, fps, fns, thr[1]

    # Classification data.
    rates = {}  # Dictionary.
    rates['recall'] = data[:, 0]  # Recall.
    rates['precision'] = data[:, 1]  # Precision.
    rates['f_measure'] = data[:, 2]  # F-measure.
    rates['thresholds'] = data[:, 6]  # Thresholds.
    rates['true_positives'] = data[:, 3]  # True positives.
    rates['false_positives'] = data[:, 4]  # False positives.
    rates['false_negatives'] = data[:, 5]  # False negatives.

    return rates

# Equal Error Rate (EER).
def fun_equal_error_rate(rates):
    ''' This function computes the Equal Error Rate (EER). '''

    # Recall, precision and threshold.
    rec = rates['recall']
    pre = rates['precision']
    thr = rates['thresholds']

    # Computation of EER.
    eer_ind = np.argmin(np.abs((pre-rec)))
    eer_val = 0.5*(pre[eer_ind] + rec[eer_ind])
    eer_thr = thr[eer_ind]

    # Equal error rate.
    eer = {'value':eer_val, 'threshold':eer_thr, 'index': eer_ind}

    return eer

# Classification results.
def fun_classification_results(scores, labels):
    ''' This function computes the classification results: distance between
    class distributions and classification rates. '''

    # Classification rates.
    rates = fun_classification_rates(scores, labels)

    # Equal Error Rate (EER).
    eer = fun_equal_error_rate(rates)

    # Score-class distribution distance.
    distance = fun_score_distribution_distance(scores, labels)

    return rates, eer, distance

# Main.
def fun_main():
    '''This file includes some functions to evaluate the classifier. '''

    # Message.
    utils.fun_message('heading','Evaluation')

    # Example.
    utils.fun_message('section','Example: Hellinger distance')
    utils.fun_message('process','Compute the distance between two-class ' \
                      '(Gaussian) distributions')
    utils.fun_message('process','pos_miu=0.8, neg_miu=0.4, pos_var=0.02,'\
                      'neg_var=0.01')
    utils.fun_message('process', 'fun_hellinger_distance(pos_miu, neg_miu,'\
                      'pos_var, neg_var)')

    # Hellinger distance between to Gaussian distributions.
    distance = fun_hellinger_distance(0.8, 0.4, 0.02, 0.01)

    # Message.
    utils.fun_message('info','Distance: {0:3f}'.format(distance))

if __name__=='__main__':
    fun_main()
