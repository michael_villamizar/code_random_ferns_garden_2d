import time
import numpy as np
import utils as utils
import evaluation as eva
import visualization as vis
import samples_2d as dataset
import random_ferns_2d as rfs
from numpy import random as rnd

# Initialization.
def fun_init(garden, num_weak_classifiers=10):
    ''' This function initializes the online boosted random ferns classifier
    with the input garden -pool of random ferns-. The most discriminative
    random ferns from the garden are chosen via online boosting to build the
    classifier.'''

    # Variables.
    num_feats = garden['num_feats']  # Number of binary features.
    garden_size = garden['num_ferns']  # Number of ferns in the garden.

    # Allocate initial ferns distributions -uniform histograms-.
    num_bins = 2**num_feats  # Number of histogram bins.
    hstms = np.zeros((num_weak_classifiers, num_bins), dtype=float)
    pos_hstms = np.ones((garden_size, num_bins), dtype=float) # Positive distr.
    neg_hstms = np.ones((garden_size, num_bins), dtype=float) # Negative distr.
    rat_hstms = 0.5*np.ones((garden_size, num_bins), dtype=float)  # Ratio.

    # Correct and wrong learning weights: lambdas.
    corr_lambdas = np.ones((garden_size, num_weak_classifiers), dtype=float)
    wrng_lambdas = np.ones((garden_size, num_weak_classifiers), dtype=float)

    # Ferns selection. Initially, the ferns are chosen from the garden at
    # random, but then they are selected online from the garden using boosting.
    rnd_indexes = range(garden_size)  # Random indexes.
    rnd.shuffle(rnd_indexes)  # Shuffle.
    rnd_indexes = rnd_indexes[:num_weak_classifiers]  # Select the first ferns.
    ferns = garden['ferns'][rnd_indexes, :, :]  # Extract random ferns.

    # Classifier info.
    info = {'num_weak_classifiers':num_weak_classifiers, \
            'num_feats':num_feats, 'num_bins':num_bins}

    # Classifier: online boosted random ferns.
    clfr = {}  # classifier
    clfr['tag'] = 'obrfs'  # Classifier tag.
    clfr['info'] = info  # Classifier information.
    clfr['ferns'] = ferns  # Random ferns.
    clfr['hstms'] = hstms  # Fern probabilities.
    clfr['pos_hstms'] = pos_hstms  # Positive fern probabilities.
    clfr['neg_hstms'] = neg_hstms  # Negative ferns probabilities.
    clfr['rat_hstms'] = rat_hstms  # Ratio of fern probabilities.
    clfr['corr_lambdas'] = corr_lambdas  # Correct weights.
    clfr['wrng_lambdas'] = wrng_lambdas  # Wrong weights.

    return clfr

# Update the classifier.
def fun_update(clfr, garden, sample, label, max_lambda=100):
    ''' This function updates the online boosted random ferns classifier with
    the input sample and its corresponding label. This function uses online
    boosting to select the most discriminative ferns from the input garden.
    This is done online adapting the sample importance weight lambda. '''

    # Assert -check classifier-.
    assert clfr['tag']=='obrfs'

    # Fern garden outputs.
    garden_outputs = rfs.fun_test_fern_garden_2d(garden, sample)

    # Variables.
    lambdaa = 1.0  # Sample weight.
    indexes = []  # Selected fern indexes.

    label = np.copy(label)
    if label == 0:
        label = -1.0
    else:
        label = 1.0

    # Update distributions for all ferns in the garden.
    for r in range(garden['num_ferns']):

        # Fern output.
        z = garden_outputs[r]

        # Update ferns distributions.
        if label==+1: clfr['pos_hstms'][r, z] += lambdaa
        if label==-1: clfr['neg_hstms'][r, z] += lambdaa
        clfr['rat_hstms'][r, z] = float(clfr['pos_hstms'][r, z])/ \
                float(clfr['pos_hstms'][r, z] + clfr['neg_hstms'][r, z])

    # Weak classifiers.
    for w in range(clfr['info']['num_weak_classifiers']):

        # Variables.
        e_plus = 1  # Best classification error.
        r_plus = -1  # Best fern index.

        # Garden ferns.
        for r in range(garden['num_ferns']):

            # Fern output.
            z = garden_outputs[r]

            # Weak classifier output.
            h_r = clfr['rat_hstms'][r, z]
            h_r = 2.0*(h_r - 0.5)

            # Assert.
            assert 1.0>=h_r>=-1.0

            # Update lambda weights.
            if h_r*label>0:
                # Correct classification weight.
                clfr['corr_lambdas'][r, w] += lambdaa
            else:
                # Incorrect classification weight.
                clfr['wrng_lambdas'][r, w] += lambdaa

            # Error computation.
            e = clfr['wrng_lambdas'][r, w]/(clfr['wrng_lambdas'][r, w]\
                                            + clfr['corr_lambdas'][r, w])
            # Assert.
            assert 1.0>=e>=0.0

            # Best fern.
            if e<e_plus:
                # Not repeated fern.
                if not r_plus in indexes:
                    # Update values.
                    e_plus = e  # Best classification error.
                    r_plus = r  # Best fern index.
        # Assert.
        assert not r_plus==-1

        # Fern output.
        z = garden_outputs[r_plus]

        # Weak classifier output.
        h_plus = clfr['rat_hstms'][r_plus, z]
        h_plus = 2.0*(h_plus - 0.5)

        # Update sample weight.
        if  h_plus*label>0:
            lambdaa = float(lambdaa)/(2.0*(1-e_plus))
        else:
            lambdaa = float(lambdaa)/(2.0*e_plus)

        # Control large lambda values due to potential outliers.
        lambdaa = np.minimum(lambdaa, max_lambda)

        # Update fern indexes.
        indexes.append(r_plus)

        # Update classifier.
        clfr['hstms'][w, :] = clfr['rat_hstms'][r_plus, :]
        clfr['ferns'][w, :, :] = garden['ferns'][r_plus, :, :]

# Train the online boosted random ferns classifier.
def fun_train(clfr, garden, samples, labels):
    ''' This function trains the online boosted random ferns classifier. This
    classifier is computed online by selecting via boosting the most
    discriminative random ferns for classification. The ferns are chosen from a
    pool of random ferns -garden-. '''

    # Assert -classifier type-.
    assert clfr['tag']=='obrfs'

    # Number of samples.
    num_samples = samples.shape[1]

    # Assert -number of samples-.
    assert num_samples==len(labels)

    # Time.
    t0 = time.time()

    # Message.
    utils.fun_message('process','Online learning')

    # Samples.
    for n in range(num_samples):

        # Update the classifier.
        fun_update(clfr, garden, samples[:, n], labels[n])

        # Message.
        if n%200==0:
            utils.fun_message('info','Sample: {}/{}'.format(n, num_samples))

    # Training time.
    trn_time = time.time()- t0

    return trn_time

# Classifier computation.
def fun_compute(garden, samples, labels, num_weak_classifiers=10):
    ''' This function computes -initialize and train- the online boosted random
    ferns classifier.'''

    # Initialize the classifier.
    clfr = fun_init(garden, num_weak_classifiers=num_weak_classifiers)

    # Train the classifier.
    trn_time = fun_train(clfr, garden, samples, labels)

    return clfr, trn_time

# Test the classifier.
def fun_test_classifier(clfr, sample):
    ''' This function tests the classifier on the input sample. '''

    # Assert -check classifier-.
    assert clfr['tag']=='obrfs'

    # Ferns outputs.
    ferns_outputs = rfs.fun_test_random_ferns_2d(clfr['ferns'], sample)

    # Variables.
    score = 0.0  # Classification score.

    # Test weak classifiers.
    for w in range(clfr['info']['num_weak_classifiers']):

        # Fern output.
        z = ferns_outputs[w]

        # Classifier confidence.
        score += clfr['hstms'][w, z]

    # Normalization.
    score /= clfr['info']['num_weak_classifiers']

    return score

# Test stage.
def fun_test(clfr, samples):
    ''' This function tests the classifier on the input set of samples. '''

    # Assert -check classifier-.
    assert clfr['tag']=='obrfs'

    # Time.
    t0 = time.time()

    # Number of test samples.
    num_samples = samples.shape[1]

    # Test the classifier on the samples.
    scores = np.array([fun_test_classifier(clfr, samples[:, n]) \
                       for n in range(num_samples)])

    # Test time.
    tst_time = time.time()- t0

    return scores, tst_time

# Main.
def fun_main():
    ''' This function computes the Online Boosted Random Ferns classifier
    (OBRFs) for the classification of 2D samples belonging to two different
    classes. '''

    # Message.
    utils.fun_message('heading', 'Online Boosted Random Ferns Classifier')
    utils.fun_message('section', 'Online Boosted Random Ferns Classifier')

    # Parameters:
    num_feats = 8  # Number of binary features.
    num_weak_classifiers = 50  # Number of weak classifiers.
    garden_size = 200  # Number of ferns in the garden -ferns pool-.

    # Train and test samples.
    trn_samples, trn_labels, num_classes = dataset.fun_example_2()
    tst_samples, tst_labels, num_classes = dataset.fun_example_2()

    # Computation of the fern "garden" -pool of random garden-.
    garden = rfs.fun_fern_garden_2d(num_ferns=garden_size, num_feats=num_feats)

    # Computation of the classifier.
    utils.fun_message('process','Initializing and training...')
    clfr, trn_time = fun_compute(garden, trn_samples, trn_labels, \
                                 num_weak_classifiers=num_weak_classifiers)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = fun_test(clfr, tst_samples)

    # Classification results.
    utils.fun_message('process','Evaluating the classifier...')
    rates, eer, distance = eva.fun_classification_results(scores, tst_labels)

    # Messages.
    utils.fun_message('process', 'Results:')
    utils.fun_message('info', 'Equal error rate: {0:.3f}'.format(eer['value']))
    utils.fun_message('info', 'Classifier threshold: {0:.3f}'. \
                      format(eer['threshold']))
    utils.fun_message('info', 'Distance between classes: {0:.3f}'. \
                      format(distance))
    utils.fun_message('info', 'Training time: {0:.3f} [sec.]'.format(trn_time))
    utils.fun_message('info', 'Test time: {0:.3f} [sec.]'.format(tst_time))

    # Results -visualization-.
    vis.fun_show_scores(scores, tst_labels)  # Classifier confidence.
    vis.fun_show_rp_curve(rates)  # Recall-precision curve.
    vis.fun_show_f_score(rates)  # F-measure/score.
    vis.fun_show_rates(rates)  # True/false positives and false negative rates.
    vis.fun_show_classification_map(clfr, fun_test)  # Classification maps.
    vis.fun_show_results(tst_samples, tst_labels, scores, eer['threshold'])
    vis.fun_show_class_distributions(scores, tst_labels) # Score distributions.

if __name__=='__main__':
    fun_main()
