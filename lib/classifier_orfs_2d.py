import time
import numpy as np
import utils as utils
import evaluation as eva
import visualization as vis
import samples_2d as dataset
import random_ferns_2d as rfs
from numpy import random as rnd

# Initialization.
def fun_init(garden, num_ferns=10):
    ''' This function initializes the online random ferns classifier with the
    input garden -pool of random ferns-. A random subset of ferns is chosen
    from the garden to compute the classifier.'''

    # Variables.
    num_feats = garden['num_feats']  # Number of binary features.
    garden_size = garden['num_ferns']  # Number of ferns in the garden.

    # Allocate initial ferns distributions -uniform histograms-.
    num_bins = 2**num_feats  # Number of histogram bins.
    pos_hstms = np.ones((num_ferns, num_bins), dtype=int)  # Positive distr.
    neg_hstms = np.ones((num_ferns, num_bins), dtype=int)  # Negative distr.
    rat_hstms = 0.5*np.ones((num_ferns, num_bins), dtype=float)  # Ratio distr.

    # Ferns selection. The ferns are chosen from the garden at random.
    rnd_indexes = range(garden_size)  # Random indexes.
    rnd.shuffle(rnd_indexes)  # Shuffle.
    rnd_indexes = rnd_indexes[:num_ferns]  # Select the first ferns.
    ferns = garden['ferns'][rnd_indexes, :, :]  # Extract random ferns.

    # Classifier info.
    info = {'num_ferns':num_ferns, 'num_feats':num_feats, 'num_bins':num_bins}

    # Classifier: online random ferns.
    clfr = {}  # classifier
    clfr['tag'] = 'orfs'  # Classifier tag.
    clfr['info'] = info  # Classifier information.
    clfr['ferns'] = ferns  # Random ferns.
    clfr['pos_hstms'] = pos_hstms  # Positive fern probabilities.
    clfr['neg_hstms'] = neg_hstms  # Negative fern probabilities.
    clfr['rat_hstms'] = rat_hstms  # Ratio fern probabilities.

    return clfr

# Update the classifier.
def fun_update(clfr, sample, label):
    ''' This function updates the online random ferns classifier with the input
    sample and its corresponding label. '''

    # Assert -check classifier-.
    assert clfr['tag']=='orfs'

    # Ferns outputs.
    ferns_outputs = rfs.fun_test_random_ferns_2d(clfr['ferns'], sample)

    # Update ferns distributions.
    for r in range(clfr['info']['num_ferns']):

        # Fern output.
        z = ferns_outputs[r]

        # Update ferns distributions.
        if label==1: clfr['pos_hstms'][r, z] += 1
        if label==0: clfr['neg_hstms'][r, z] += 1
        clfr['rat_hstms'][r, z] = float(clfr['pos_hstms'][r, z])/ \
                float(clfr['pos_hstms'][r, z] + clfr['neg_hstms'][r, z])

# Train the online random ferns classifier.
def fun_train(clfr, samples, labels):
    ''' This function trains the online random ferns classifier from the
    combination of random ferns which are updated incrementally with the
    training samples.'''

    # Assert -classifier type-.
    assert clfr['tag']=='orfs'

    # Number of samples.
    num_samples = samples.shape[1]

    # Assert -number of samples-.
    assert num_samples==len(labels)

    # Time.
    t0 = time.time()

    # Message.
    utils.fun_message('process','Online learning')

    # Samples.
    for n in range(num_samples):

        # Update the classifier.
        fun_update(clfr, samples[:, n], labels[n])

        # Message.
        if n%200==0:
            utils.fun_message('info','Sample: {}/{}'.format(n, num_samples))

    # Training time.
    trn_time = time.time()- t0

    return trn_time

# Classifier computation.
def fun_compute(garden, samples, labels, num_ferns=10):
    ''' This function computes -initialize and train- the online random ferns
    classifier.'''

    # Initialize the classifier.
    clfr = fun_init(garden, num_ferns=num_ferns)

    # Train the classifier.
    trn_time = fun_train(clfr, samples, labels)

    return clfr, trn_time

# Test the classifier.
def fun_test_classifier(clfr, sample):
    ''' This function tests the classifier on the input sample. '''

    # Assert -check classifier-.
    assert clfr['tag']=='orfs'

    # Ferns outputs.
    ferns_outputs = rfs.fun_test_random_ferns_2d(clfr['ferns'], sample)

    # Variables.
    score = 0.0

    # Test ferns.
    for r in range(clfr['info']['num_ferns']):

        # Fern output.
        z = ferns_outputs[r]

        # Classifier confidence.
        score += clfr['rat_hstms'][r, z]

    # Normalization.
    score /= clfr['info']['num_ferns']

    return score

# Test stage.
def fun_test(clfr, samples):
    ''' This function tests the classifier on the input set of samples. '''

    # Assert -check classifier-.
    assert clfr['tag']=='orfs'

    # Time.
    t0 = time.time()

    # Number of test samples.
    num_samples = samples.shape[1]

    # Test the classifier on the samples.
    scores = np.array([fun_test_classifier(clfr, samples[:, n]) \
                       for n in range(num_samples)])

    # Test time.
    tst_time = time.time()- t0

    return scores, tst_time

# Main.
def fun_main():
    ''' This function computes the Online Random Ferns classifier (ORFs) for
    the classification of 2D samples belonging to two different classes. '''

    # Message.
    utils.fun_message('heading', 'Online Random Ferns Classifier')
    utils.fun_message('section', 'Online Random Ferns Classifier')

    # Parameters:
    num_feats = 8  # Number of binary features.
    num_ferns = 50  # Number of random ferns.
    garden_size = 200  # Number of ferns in the garden -ferns pool-.

    # Train and test samples.
    trn_samples, trn_labels, num_classes = dataset.fun_example_2()
    tst_samples, tst_labels, num_classes = dataset.fun_example_2()

    # Computation of the fern "garden" -pool of random garden-.
    garden = rfs.fun_fern_garden_2d(num_ferns=garden_size, num_feats=num_feats)

    # Computation of the classifier.
    utils.fun_message('process','Initializing and training...')
    clfr, trn_time = fun_compute(garden, trn_samples, trn_labels, \
                                 num_ferns=num_ferns)
    # Test the classifier.
    utils.fun_message('process','Testing...')
    scores, tst_time = fun_test(clfr, tst_samples)

    # Classification results.
    utils.fun_message('process','Evaluating the classifier...')
    rates, eer, distance = eva.fun_classification_results(scores, tst_labels)

    # Messages.
    utils.fun_message('process', 'Results:')
    utils.fun_message('info', 'Equal error rate: {0:.3f}'.format(eer['value']))
    utils.fun_message('info', 'Classifier threshold: {0:.3f}'. \
                      format(eer['threshold']))
    utils.fun_message('info', 'Distance between classes: {0:.3f}'. \
                      format(distance))
    utils.fun_message('info', 'Training time: {0:.3f} [sec.]'.format(trn_time))
    utils.fun_message('info', 'Test time: {0:.3f} [sec.]'.format(tst_time))

    # Results -visualization-.
    vis.fun_show_scores(scores, tst_labels)  # Classifier confidence.
    vis.fun_show_rp_curve(rates)  # Recall-precision curve.
    vis.fun_show_f_score(rates)  # F-measure/score.
    vis.fun_show_rates(rates)  # True/false positives and false negative rates.
    vis.fun_show_classification_map(clfr, fun_test)  # Classification maps.
    vis.fun_show_results(tst_samples, tst_labels, scores, eer['threshold'])
    vis.fun_show_class_distributions(scores, tst_labels) # Score distributions.

if __name__=='__main__':
    fun_main()
