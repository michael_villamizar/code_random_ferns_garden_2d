import numpy as np
import matplotlib.pyplot as plt
from numpy import random as rnd

# Normalize samples data.
def fun_normalize_data(samples):
    ''' This function normalizes the 2D samples data in the range [0, 1].'''

    # Assert.
    assert samples.ndim==2

    # Normalizing data in [0, 1]
    samples[0, :] = samples[0, :] -np.min(samples[0, :])
    samples[0, :] = samples[0, :]/np.max(samples[0, :])
    samples[1, :] = samples[1, :] -np.min(samples[1, :])
    samples[1, :] = samples[1, :]/np.max(samples[1, :])

# Shuffle samples and labels.
def fun_shuffle(samples, labels):
    ''' This function shuffles the samples and labels. '''

    # Assert.
    assert samples.shape[1]==len(labels)

    # Number of samples.
    num_samples = samples.shape[1]

    # Shuffle.
    indx = np.arange(num_samples)
    np.random.shuffle(indx)
    labels = labels[indx]
    samples = samples[:, indx]

    return samples, labels

# Check output.
def fun_check_output(samples, labels, num_classes):
    ''' This functions checks that the output (samples, labels and number of
    classes) is correct. '''

    # Flag.
    success = False

    # Min and max values
    x0_min = np.min(samples[0, :])
    x0_max = np.max(samples[0, :])
    x1_min = np.min(samples[1, :])
    x1_max = np.max(samples[1, :])

    # Check size, number of classes, data type and normalization.
    if samples.shape[1]==len(labels):
        if num_classes-1==np.max(labels):
            if samples.dtype==float:
                if labels.dtype==int:
                    if x0_min>=0 and x1_min>=0:
                        if x1_max<=1 and x1_max<=1:
                            success = True
    return success

# Samples: Example 1.
def fun_example_1(num_pos_samples=1000, num_neg_samples=1000):
    ''' This function computes samples for scenario example 1. This example
    corresponds to two -unimodal- Gaussian distributions.'''

    # Parameters.
    pos_mean = (0.6, 0.6)  # Positive class mean.
    neg_mean = (0.3, 0.3)  # Negative class mean.
    pos_cov = [[0.01, -0.015], [-0.005, 0.01]]  # Positive class covariance.
    neg_cov = [[0.008, 0], [0, 0.008]]  # Negative class covariance.
    num_classes = 2  # Number of classes.

    # Positive and negative samples.
    pos_x0, pos_x1 = rnd.multivariate_normal(pos_mean, pos_cov, \
                                                 num_pos_samples).T
    neg_x0, neg_x1 = rnd.multivariate_normal(neg_mean, neg_cov, \
                                                 num_neg_samples).T
    # Stack.
    pos_samples = np.vstack((pos_x0, pos_x1))
    neg_samples = np.vstack((neg_x0, neg_x1))
    samples = np.hstack((pos_samples, neg_samples))

    # Normalize samples data.
    fun_normalize_data(samples)

    # Labels.
    pos_labels = np.ones(num_pos_samples, dtype=int);
    neg_labels = np.zeros(num_neg_samples, dtype=int);
    labels = np.hstack((pos_labels, neg_labels))

    # Shuffle samples and labels.
    samples, labels = fun_shuffle(samples, labels)

    # Assert.
    assert fun_check_output(samples, labels, num_classes)==True

    return samples, labels, num_classes

# Samples: Example 2.
def fun_example_2(num_pos_samples=1000, num_neg_samples=1000):
    ''' This function computes samples for scenario example 2. This example
    contains two-class sample distributions with a spiral layout.'''

    # Parameters.
    start = 50 # Determines how far from the origin the spirals start(degrees).
    noise = 1.5  # Displaces the instances from the spiral.
    degrees = 450  # Controls the length of the spirals.
    num_classes = 2  # Number of classes.

    # Degrees.
    deg2rad = float(2*np.pi)/360.0
    start = start * deg2rad

    # Spiral 1 -positive samples-.
    theta = start + np.sqrt(rnd.random((1, num_pos_samples))) * degrees*deg2rad
    x0 = -np.sin(theta)*theta + rnd.random((1, num_pos_samples))*noise
    x1 = np.cos(theta)*theta + rnd.random((1, num_pos_samples))*noise
    pos_samples = np.vstack((x0, x1))

    # Spiral 2 -negative samples-.
    theta = start + np.sqrt(rnd.random((1, num_neg_samples))) * degrees*deg2rad
    x0 = np.sin(theta)*theta + rnd.random((1, num_neg_samples))*noise
    x1 = -np.cos(theta)*theta + rnd.random((1, num_neg_samples))*noise
    neg_samples = np.vstack((x0, x1))

    # Stack.
    samples = np.hstack((pos_samples, neg_samples))

    # Normalize samples data.
    fun_normalize_data(samples)

    # Labels.
    pos_labels = np.ones(num_pos_samples, dtype=int);
    neg_labels = np.zeros(num_neg_samples, dtype=int);
    labels = np.hstack((pos_labels, neg_labels))

    # Shuffle samples and labels.
    samples, labels = fun_shuffle(samples, labels)

    # Assert.
    assert fun_check_output(samples, labels, num_classes)==True

    return samples, labels, num_classes

# Samples: Example 3.
def fun_example_3(num_pos_samples=1000, num_neg_samples=1000):
    '''This function computes samples for scenario example 3. This example
    contains two class distributions with a non-linear layout. The positive
    class distribution -Gaussian- is at the feature space center whereas the
    negative distribution presents a ring-like layout.'''

    # Parameters.
    radius_1 = 0.3  # Class radius 1.
    radius_2 = 0.5  # Class radius 2.
    num_classes = 2 # Number of classes.

    # Positives samples.
    radius = radius_2*rnd.random((1, num_neg_samples))
    angle = 2*np.pi*rnd.random((1, num_neg_samples))
    x0 = np.cos(angle)*radius
    x1 = np.sin(angle)*radius
    pos_samples = np.vstack((x0, x1))

    # Negative samples.
    neg_radius = radius_2 + radius_1*rnd.random((1, num_neg_samples))
    neg_angle = 2*np.pi*rnd.random((1, num_neg_samples))
    x0 = np.cos(neg_angle)*neg_radius
    x1 = np.sin(neg_angle)*neg_radius
    neg_samples = np.vstack((x0, x1))

    # Stack.
    samples = np.hstack((pos_samples, neg_samples))

    # Normalize samples data.
    fun_normalize_data(samples)

    # Labels.
    pos_labels = np.ones(num_pos_samples, dtype=int);
    neg_labels = np.zeros(num_neg_samples, dtype=int);
    labels = np.hstack((pos_labels, neg_labels))

    # Shuffle samples and labels.
    samples, labels = fun_shuffle(samples, labels)

    # Assert.
    assert fun_check_output(samples, labels, num_classes)==True

    return samples, labels, num_classes

# Show samples.
def fun_show_samples(samples, labels, num_classes):
    ''' This function shows the positive and negative samples in the 2D feature
    space. '''

    # Assert.
    assert samples.shape[1]==len(labels)

    # Plot samples.
    for c in range(num_classes):
        indx = labels==c
        plt.plot(samples[0, indx], samples[1, indx], 'o')

    # Plot figure.
    plt.axis('equal')
    plt.xlabel('x0')
    plt.ylabel('x1')
    plt.grid()
    plt.show()

# Main function.
def fun_main():
    ''' The main function computes the 2D samples for the example 1.'''

    # Samples: example 1.
    samples, labels, num_classes = fun_example_1(10,10)
    print labels.T

    # Show samples.
    fun_show_samples(samples, labels, num_classes)

if __name__=='__main__':
    fun_main()
